<?php
	$this->pageTitle = 'หน้าหลัก' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="row">
                                            <!-- Material statustic card start -->
                                            <div class="col-md-12">
                                                <div class="card mat-stat-card">
                                                    <div class="card-block">             
                                                        <div class="text-center">
                                                            <div class="baner">
                                                                <marquee><span>
                                                                    <b style="color:red;">THAILANDSLOT.COM</b> 
                                                                    ฝาก - ออนเร็วกันใจ แอดมินพร้อมบริการตลอด <b style="color:blue;">24</b> ชม.
                                                                </span></marquee>
                                                            </div>
                                                            <div id="myCarousel" class="carousel slide" data-ride="carousel" >
                                                                <ol class="carousel-indicators">
                                                                	<?php    
																		$data=lookupdata::getImages("slide");																		
																		for ($x = 0; $x < count($data); $x++) {
																			if($x==0){
																				echo '<li data-target="#myCarousel" data-slide-to="'.$x.'" class="active"></li>';
																			}
																			else{
																				echo '<li data-target="#myCarousel" data-slide-to="'.$x.'"></li>';
																			}
																			
																		}
																	 ?>
                                                                </ol>
                                                                
                                                                <div class="carousel-inner crop">
                                                                	<?php    
																		$data=lookupdata::getImages("slide");																		
																		for ($x = 0; $x < count($data); $x++) {
																			if($x==0){
																				echo '<div class="item active">
																						<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" />
																					</div>
																				';
																			}
																			else{
																				echo '<div class="item">
																						<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" />
																					</div>
																				';
																			}
																			
																		}
																	 ?>
                                                                    <!-- Left and right controls -->
                                                                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
														
                                                        
														
														
														
                                                        <div class="text-center" data-aos="flip-left" data-aos-easing="ease-out-cubic"data-aos-duration="2000">
															<img src="image/title/Promotion.png" class="topic-page-first" />
														</div>
                                                        <div class="text-center hide-mobile">
                                                            <div  id="myCarousel2" class="carousel slide main-box" data-ride="carousel" >
																<ol class="carousel-indicators">
																	<?php    
																		$data=lookupdata::getImages("promotion");																		
																		for ($x = 0; $x < count($data); $x++) {
																			if($x==0){
																				echo '<li data-target="#myCarousel2" data-slide-to="'.$x.'" class="active"></li>';
																			}
																			else{
																				echo '<li data-target="#myCarousel2" data-slide-to="'.$x.'"></li>';
																			}																		
																		}
																	?>  
																</ol>

																<div class="carousel-inner">
																	
																	<?php    
																		$data=lookupdata::getImages("promotion");																			
																		for ($x = 0; $x < count($data); $x++) {
																			if($x==0){
																				echo '<div class="item active">
																						<div class="text-center hide-mobile" style="display:flex;">
																							<img class="hovOpa box-two" src="'.$data[0]['url'].'" >
																							<img class="hovOpa box-two" src="'.$data[1]['url'].'" >
																							
																						</div>
																					</div>';
																			}
																			if($x==1){
																				echo '<div class="item">
																						<div class="text-center hide-mobile" style="display:flex;">
																							<img class="hovOpa box-two" src="'.$data[2]['url'].'" >
																							<img class="hovOpa box-two" src="'.$data[3]['url'].'" >
																							
																						</div>
																					</div>';
																			}
																			if($x==2){
																				echo '<div class="item">
																						<div class="text-center hide-mobile" style="display:flex;">
																							<img class="hovOpa box-two" src="'.$data[4]['url'].'" >
																							<img class="hovOpa box-two" src="'.$data[5]['url'].'" >
																							
																						</div>
																					</div>';
																			}
																			else
																			{
																				echo '<div class="item">
																						<div class="text-center hide-mobile" style="display:flex;">
																							<img class="hovOpa box-two" src="'.$data[6]['url'].'" >
																							<img class="hovOpa box-two" src="'.$data[1]['url'].'" >
																							
																						</div>
																					</div>';
																			}																		
																		}
																	?>   
																	

																	<!-- Left and right controls -->
																	<a class="left carousel-control" href="#myCarousel2" data-slide="prev">
																		<span class="glyphicon glyphicon-chevron-left"></span>
																		<span class="sr-only">Previous</span>
																	</a>
																	<a class="right carousel-control" href="#myCarousel2" data-slide="next">
																		<span class="glyphicon glyphicon-chevron-right"></span>
																		<span class="sr-only">Next</span>
																	</a>
																</div>
															</div>
														</div>

                                                        <!-- respone mobile promotion -->
                                                        <div id="myCarousel21" class="carousel slide show-mobile-slider padding20" data-ride="carousel" >
                                                            <ol class="carousel-indicators">
                                                            	<?php    
																	$data=lookupdata::getImages("promotion");																		
																	for ($x = 0; $x < count($data); $x++) {
																		if($x==0){
																			echo '<li data-target="#myCarousel21" data-slide-to="'.$x.'" class="active"></li>';
																		}
																		else{
																			echo '<li data-target="#myCarousel21" data-slide-to="'.$x.'"></li>';
																		}																		
																	}
																?>                                                                
                                                            </ol>
                                                            
                                                            <div class="carousel-inner crop">
																
                                                            	<?php    
																	$data=lookupdata::getImages("promotion");																			
																	for ($x = 0; $x < count($data); $x++) {
																		if($x==0){
																			echo '<div class="item active">
																					<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" >
																				
																				</div>';
																		}
																		else{
																			echo '<div class="item">
																					<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" >
																				</div>';
																		}																		
																	}
																?>   
																<!-- Left and right controls -->
																<a class="left carousel-control" href="#myCarousel21" data-slide="prev">
																	<span class="glyphicon glyphicon-chevron-left"></span>
																	<span class="sr-only">Previous</span>
																</a>
																<a class="right carousel-control" href="#myCarousel21" data-slide="next">
																	<span class="glyphicon glyphicon-chevron-right"></span>
																	<span class="sr-only">Next</span>
																</a>
                                                            </div>
                                                        </div>
                                                        <!-- end -->

                                                        <div class="text-center" data-aos="flip-left"data-aos-easing="ease-out-cubic" data-aos-duration="2000">
															<img src="image/title/Bigwin.png" class="topic-page-first" />
														</div>
                                                        <div class="text-center hide-mobile">
                                                            <div class="main-box" >
                                                            	<?php    
																	$data=lookupdata::getImages("bigwin");																			
																	for ($x = 0; $x < count($data); $x++) {
																		echo '<img class="box-two" src="'.$data[$x]['url'].'" >';
																																			
																	}
																?>   
                                                            </div>
														</div>

                                                        <!-- respone mobile bigwin -->
                                                        <div id="myCarousel3" class="carousel slide show-mobile-slider padding20" data-ride="carousel" >
                                                            <ol class="carousel-indicators">
                                                            	<?php    
																	$data=lookupdata::getImages("bigwin");																		
																	for ($x = 0; $x < count($data); $x++) {
																		if($x==0){
																			echo '<li data-target="#myCarousel3" data-slide-to="'.$x.'" class="active"></li>';
																		}
																		else{
																			echo '<li data-target="#myCarousel3" data-slide-to="'.$x.'"></li>';
																		}																		
																	}
																?>         
                                                            </ol>
                                                            
                                                            <div class="carousel-inner crop">
                                                            	<?php    
																	$data=lookupdata::getImages("bigwin");																			
																	for ($x = 0; $x < count($data); $x++) {
																		if($x==0){
																			echo '<div class="item active">
																					<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" >
																				</div>';
																		}
																		else{
																			echo '<div class="item">
																					<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" >
																				</div>';
																		}																		
																	}
																?>   
                                                               
                                                            
                                                                <!-- Left and right controls -->
																	<a class="left carousel-control" href="#myCarousel3" data-slide="prev">
																		<span class="glyphicon glyphicon-chevron-left"></span>
																		<span class="sr-only">Previous</span>
																	</a>
																	<a class="right carousel-control" href="#myCarousel3" data-slide="next">
																		<span class="glyphicon glyphicon-chevron-right"></span>
																		<span class="sr-only">Next</span>
																	</a>
                                                            </div>
                                                        </div>
                                                        <!-- end -->



                                                        <div class="text-center" data-aos="flip-left"data-aos-easing="ease-out-cubic" data-aos-duration="2000">
															<img src="image/title/หัวข้อ.png" class="bigwin" />
														</div>
                                                        <div class="text-cente hide-mobile">
                                                            <div class="game-hit">
                                                                <div class="row game">
																	<?php    
																		$data=lookupdata::getImages("game");																			
																		for ($x = 0; $x < count($data); $x++) {
																			if($x==0){
																				echo '<div class="col-md-2 col-xl-2">
																						
																							<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" >
																						
																					</div>';
																			}
																			else{
																				echo '<div class="col-md-2 col-xl-2">
																						
																							<img class="hovOpa slider-img" src="'.$data[$x]['url'].'" >
																						
																					</div>';
																			}																		
																		}
																	?>   
                                                                </div>
                                                            </div>
														</div>
                                                        <!-- respone mobile game hit -->
                                                        <div id="myCarousel41" class="carousel slide show-mobile-slider padding20" data-ride="carousel" >
															<ol class="carousel-indicators">
                                                            	<?php    
																	$data=lookupdata::getImages("game");																		
																	for ($x = 0; $x < count($data); $x++) {
																		if($x==0){
																			echo '<li data-target="#myCarousel41" data-slide-to="'.$x.'" class="active"></li>';
																		}
																		else{
																			echo '<li data-target="#myCarousel41" data-slide-to="'.$x.'"></li>';
																		}																		
																	}
																?>                                                                
                                                            </ol>
                                                            
                                                            <div class="carousel-inner crop">
																
                                                            	<?php    
																	$data=lookupdata::getImages("game");																			
																	for ($x = 0; $x < count($data); $x++) {
																		if($x==0){
																			echo '
																				<div class="row item active">
																					<div class="col-md-6 col-xl-6 col-xs-6 ">
																						<img class="hovOpa slider-img" src="'.$data[0]['url'].'" >
																						
																					</div>
																					<div class="col-md-6 col-xl-6 col-xs-6">
																						<img class="hovOpa slider-img" src="'.$data[1]['url'].'" >
																						
																					</div>
																				</div>
																				';
																		}
																		if($x==1){
																			echo '<div class="row item">
																				<div class="col-md-6 col-xl-6 col-xs-6 ">
																					<img class="hovOpa slider-img" src="'.$data[2]['url'].'" >
																					
																				</div>
																				<div class="col-md-6 col-xl-6 col-xs-6">
																					<img class="hovOpa slider-img" src="'.$data[3]['url'].'" >
																					
																				</div>
																			</div>';
																		}																		
																		if($x==2){
																			echo '<div class="row item">
																					<div class="col-md-6 col-xl-6 col-xs-6 ">
																						<img class="hovOpa slider-img" src="'.$data[4]['url'].'" >
																						
																					</div>
																					<div class="col-md-6 col-xl-6 col-xs-6">
																						<img class="hovOpa slider-img" src="'.$data[5]['url'].'" >
																						
																					</div>
																				</div>';
																		}																		
																		if($x==3){
																			echo '<div class="row item">
																					<div class="col-md-6 col-xl-6 col-xs-6 ">
																						<img class="hovOpa slider-img" src="'.$data[6]['url'].'" >
																						
																					</div>
																					<div class="col-md-6 col-xl-6 col-xs-6">
																						<img class="hovOpa slider-img" src="'.$data[7]['url'].'" >
																						
																					</div>
																				</div>';
																		}																		
																		if($x==4){
																			echo '<div class="row item">
																					<div class="col-md-6 col-xl-6 col-xs-6 ">
																						<img class="hovOpa slider-img" src="'.$data[8]['url'].'" >
																						
																					</div>
																					<div class="col-md-6 col-xl-6 col-xs-6">
																						<img class="hovOpa slider-img" src="'.$data[9]['url'].'" >
																						
																					</div>
																				</div>';
																		}																		
																		if($x==5){
																			echo '<div class="row item">
																					<div class="col-md-6 col-xl-6 col-xs-6 ">
																						<img class="hovOpa slider-img" src="'.$data[10]['url'].'" >
																						
																					</div>
																					<div class="col-md-6 col-xl-6 col-xs-6">
																						<img class="hovOpa slider-img" src="'.$data[11]['url'].'" >
																						
																					</div>
																				</div>';
																		}																		
																		if($x==6){
																			echo '<div class="row item">
																					<div class="col-md-6 col-xl-6 col-xs-6 ">
																						<img class="hovOpa slider-img" src="'.$data[12]['url'].'" >
																						
																					</div>
																					<div class="col-md-6 col-xl-6 col-xs-6">
																						<img class="hovOpa slider-img" src="'.$data[13]['url'].'" >
																						
																					</div>
																				</div>';
																		}																																																																
																		else{
																			echo '<div class="row item">
																					<div class="col-md-6 col-xl-6 col-xs-6 ">
																						<img class="hovOpa slider-img" src="'.$data[14]['url'].'" >
																						
																					</div>
																					<div class="col-md-6 col-xl-6 col-xs-6">
																						<img class="hovOpa slider-img" src="'.$data[1]['url'].'" >
																						
																					</div>
																				</div>';
																		}	
																																			
																	}
																?>   
																<!-- Left and right controls -->
																<a class="left carousel-control" href="#myCarousel41" data-slide="prev">
																	<span class="glyphicon glyphicon-chevron-left"></span>
																	<span class="sr-only">Previous</span>
																</a>
																<a class="right carousel-control" href="#myCarousel41" data-slide="next">
																	<span class="glyphicon glyphicon-chevron-right"></span>
																	<span class="sr-only">Next</span>
																</a>
																
                                                            </div>
                                                        </div>
                                                        <!-- end -->

                                                        <div class="text-center" data-aos="fade-up" style="margin-top:40px;">
															<?php    
																$data=lookupdata::getImages("article");																			
																foreach($data as $dataitem) { 
																	echo '<img src="'.$dataitem['url'].'" class="img370">';
																} 
															?>   
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>

							<div class="modal fade" id="myModal">
								<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
									<div class="modal-content bg-modal" >
										<div class="modal-header">
											<h5 class="modal-title" id="modaldetailLabel"></h5>
											<button type="button" class="btn btn-danger close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<!-- <h5>Add content here.</h5> -->
											<?php    
												$data=lookupdata::getImages("popup");																			
												foreach($data as $dataitem) { 
													echo '<img src="'.$dataitem['url'].'" style="width: 100%;" >';
												} 
											?>  
										</div>
									</div>
								</div>
							</div>
                            
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#home").addClass("active");
                                    
                                    
                                });
								//load popup
									$(window).on('load',function(){
									var delayMs = 900; // delay in milliseconds
									
									setTimeout(function(){
										$('#myModal').modal('show');
									}, delayMs);
								});
                            </script>