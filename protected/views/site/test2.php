<?php

require_once(Yii::getPathOfAlias('application') .'/vendor/Classes/PHPExcel.php');
include(Yii::getPathOfAlias('application') .'/vendor/Classes/PHPExcel/IOFactory.php');



$inputFileName = "aa.xls";  
$inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
$objReader->setReadDataOnly(true);  
$objPHPExcel = $objReader->load($inputFileName);  


$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
$highestRow = $objWorksheet->getHighestRow();
$highestColumn = $objWorksheet->getHighestColumn();

$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
$headingsArray = $headingsArray[1];

$r = 0;
$namedDataArray = array();
for ($row = 2; $row <= $highestRow; ++$row) {
    $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
        ++$r;
       	$namedDataArray[$r] = $dataRow[$row];
    }
}
$createby='1';
//echo "<table>";
			foreach($namedDataArray as $xls_value)
			{				
				
				$code = $xls_value['A'];	
				$doc_no = $xls_value['B'];	
				$doc_date = $xls_value['C'];
				//$doc_date = Yii::app()->CommonFnc->DMYth2YMD('/',$xls_value['C'],'-');
				$acc_employer = $xls_value['D'];	
				$business_name = $xls_value['E'];					
				$name = $xls_value['F'];	
				$lname = $xls_value['G'];	
				$pid = $xls_value['H'];				
				$bank_id = $xls_value['I'];	
				$bank_dep_id = $xls_value['J'];	
				$check_status = $xls_value['K'];	
				$bank_dep_name = $xls_value['L'];	
				$acc_type_id = $xls_value['M'];	
				$acc_no = $xls_value['N'];	
				$acc_name = $xls_value['O'];	
				$mark = $xls_value['P'];	
				$amont = $xls_value['Q'];	
				$request_date = $xls_value['R'];
				if(isset($xls_value['S'])){
					$remark = $xls_value['S'];
				}else{
					$remark = '';
				}				
					
				
						
				$name = trim($name, " ");	
				$lname = trim($lname, " ");
				
				$code = trim($code, " ");
				$acc_employer = trim($acc_employer, " ");
				$pid = trim($pid, " ");			
				$bank_id = trim($bank_id, " ");			
				$bank_dep_id = trim($bank_dep_id, " ");
				$check_status = trim($check_status, " ");
				$bank_dep_name = trim($bank_dep_name, " ");
				$acc_type_id = trim($acc_type_id, " ");
				$acc_no = trim($acc_no, " ");
				$acc_name = trim($acc_name, " ");	
				$mark = trim($mark, " ");		
				$amont = str_replace(',', '', $amont);
				
				//echo "<tr><td>".$code."</td><td>".$doc_no."</td><td>".$doc_date."</td><td>".$acc_employer."</td><td>".$business_name."</td><td>".$name."</td><td>".$lname."</td><td>".$pid."</td><td>".$bank_id."</td><td>".$bank_dep_id."</td></tr>";
				
				$sql = "INSERT INTO tmp ";
				$sql.= "(importresult_id,doc_no,doc_date,acc_employer,business_name,name,lname,pid,bank_id,bank_dep_id, ";
				$sql.= "check_status,bank_dep_name,acc_type_id,acc_no,acc_name,mark,amont,remark,comment, ";
				$sql.= "request_date,create_date,create_by) ";
				$sql.= "VALUES(:importresult_id,:doc_no,:doc_date,:acc_employer,:business_name,:name,:lname,:pid,:bank_id,:bank_dep_id, ";
				$sql.= ":check_status,:bank_dep_name,:acc_type_id,:acc_no,:acc_name,:mark,:amont,:remark,:comment, ";
				$sql.= ":request_date,now(),$createby) ";
				$command=yii::app()->db->createCommand($sql);		
				$command->bindValue(":importresult_id", '1');
				$command->bindValue(":doc_no", $doc_no);
				$command->bindValue(":doc_date", $doc_date);	
				$command->bindValue(":acc_employer", $acc_employer);
				$command->bindValue(":business_name", $business_name);
				$command->bindValue(":name", $name);
				$command->bindValue(":lname", $lname);
				$command->bindValue(":pid", $pid);
				$command->bindValue(":bank_id", $bank_id);				
				$command->bindValue(":bank_dep_id", $bank_dep_id); 
				$command->bindValue(":check_status", $check_status);
				$command->bindValue(":bank_dep_name", $bank_dep_name);
				$command->bindValue(":acc_type_id", $acc_type_id);
				$command->bindValue(":acc_no", $acc_no);
				$command->bindValue(":acc_name", $acc_name);
				$command->bindValue(":mark", $mark);
				$command->bindValue(":amont", $amont);
				$command->bindValue(":request_date", $request_date);
				$command->bindValue(":remark", $remark);				
				$command->bindValue(":comment", 'a');
				if($command->execute()) {
					
				} else {
					Yii::app()->session['errmsg_import']='ไม่สามารถบันทึกข้อมูลได้'.$sql;
					return false;
				}
				
				
				
				$r++;
				
			}
			
			echo "Yes";
		
		
		
		
		//echo "</table>";


?>

