<?php
	$this->pageTitle = 'โปรโมชั่น' . Yii::app()->params['prg_ctrl']['pagetitle'];	
?>
<div class="main-body">
	<div class="page-wrapper">
		<!-- Page-body start -->
		<div class="page-body">
			<div class="row">
				<!-- Material statustic card start -->
				<div class="col-md-12">
					<div class="card mat-stat-card">
						<div class="card-block ">
							<div class="text-center padding20" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
								<img src="image/title/Promotion.png" class="topic-page-first" />
							</div>
							<div class="row">
                             <?php           
								$data=lookupdata::getImages("promotion");
								$i=0;
								foreach($data as $dataitem) {  
									                       
									echo '<div class="col-md-6 box-promotion text-swap">
										<div class="padding20" data-aos="fade-down">
											<img src="'.$dataitem['url'].'" class="img370 border-gold" />
										</div>
									</div>
									';
									$i+1;
								} 	
							 ?>	
								
							</div>
							<div class="text-center padding20" data-aos="fade-up">
                            	<?php    
									$data=lookupdata::getImages("article");																			
									foreach($data as $dataitem) { 
										echo '<img src="'.$dataitem['url'].'" class="img370">';
									} 
								?>   
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Page-body end -->
	</div>
	<div id="styleSelector"> </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#promotion").addClass("active");
		
	});
</script>
	