<?php
	$this->pageTitle = 'เข้าสู่ระบบ' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>
<form class="md-float-material form-material">
	<div class="text-center">
		<img src="/image/logo.png" width="100" alt="logo.png">
	</div>
	<div class="auth-box card">
		<div class="card-block">
			<div class="row m-b-20">
				<div class="col-md-12">
					<h3 class="text-center">Sign In</h3>
				</div>
			</div>
			<div class="form-group form-primary">
				<input type="text" id="txtusername" class="form-control">
				<span class="form-bar"></span>
				<label class="float-label">ชื่อผู้ใช้</label>
			</div>
			<div class="form-group form-primary">
				<input type="password" id="txtpassword" class="form-control">
				<span class="form-bar"></span>
				<label class="float-label">รหัสผ่าน</label>
			</div>
			<div class="row m-t-30">
				<div class="col-md-12">
					<button type="button" onclick="ajax_auth()" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">เข้าสู่ระบบ</button>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-md-8">
					<p class="text-inverse text-left m-b-0">Thank you.</p>
					<p class="text-inverse text-left"><a href="home"><b>Back to website</b></a></p>
				</div>
				<div class="col-md-4">
					<img src="image/top/logothslot.png" alt="small-logo.png" width="100">
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
jQuery(document).ready(function ($) {	
	$("#txtpassword").keyup(function (e) {
		if (e.keyCode == 13) {ajax_auth();}
		});
});

function ajax_auth(){ 

	if($('#txtusername').val()==""){alert('กรุณาระบุรหัสผู้ใช้'); return;}
	if($('#txtpassword').val()==""){alert('กรุณาระบุรหัสผ่าน'); return;}
	
	var data = {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>', 'txtusername': $('#txtusername').val(), 'txtpassword': $('#txtpassword').val()}; 
	$.ajax({
		type: 'POST',
		url: '<?php echo Yii::app()->createAbsoluteUrl("signin/auth"); ?>',
		data:data,
		dataType:'json',
		//success: success_auth,
		success: function (data) {
			if (data.status=='success') {
				//location.replace("uploadimage");
				location.reload();	
			} else if(data.status=='error') { alert(data.msg); 
			} else { alert('Invalid Exception.'); } 
		},
		error: error_auth,
	});
} 
var success_auth = function (data) {
	if(data.status=='success') { location.reload();	
	} else if(data.status=='error') { alert(data.msg); 
	} else { alert('Invalid Exception.'); } 
}
var error_auth = function (data) { alert('Invalid Exception.'); }


</script>