<?php
	$this->pageTitle = 'Error '.Yii::app()->session['errmsg_code'].Yii::app()->params['prg_ctrl']['pagetitle'];

?>

<!-- Main-body start -->
<div class="main-body">
	<div class="page-wrapper">
		<!-- Page-body start -->
		<div class="page-body">
			<div class="row">
				<!-- Material statustic card start -->
				<div class="col-md-12">
					<div class="card mat-stat-card">
						<div class="card-block padding20">
							<div class="text-center padding20">
								<div class="content-login">
									 <div class="text-center">
										<img src="image/404.png" alt="" class="img-fluid">
										<h1 class="text-muted my-4">Oops! Page not found!</h1>
										<form action="home">
											<button class="btn waves-effect waves-light btn-primary mb-4"><i class="feather icon-refresh-ccw mr-2"></i>Reload</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Page-body end -->
	</div>
	<div id="styleSelector"> </div>
</div>
