<?php
	$this->pageTitle = 'ข้อมูลผู้ใช้' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>
<style>
	.float-label{
		font-size: 16px !important;
	}
</style>
 <!-- Page-header start -->
<div class="page-header"style="height: 138px;">
	<div class="page-block">
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="page-header-title">
					<h5 class="m-b-10">ข้อมูลผู้ใช้</h5>
					<p class="m-b-0">เพิ่ม แก้ไข ข้อมูลผู้ใช้</p>
				</div>
			</div>
			<div class="col-md-4">
				<button type="button" id="btnAdd" class="btn btn-success float-right waves-effect waves-light">เพิ่มข้อมูล</button>
			</div>
		</div>
	</div>
</div>
<!-- Page-header end -->
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<!-- Page body start -->
			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<!-- Bootstrap tab card start -->
						<div class="card">
							<div class="card-block">
								<!-- Row start -->
								<div class="row">
									<div class="table-responsive">
									<?php
									
									$this->widget('zii.widgets.grid.CGridView', array(
										'id' => 'list-grid',
										'dataProvider' => $model,
										'itemsCssClass' => 'table table-bordered table-striped',	
										'rowHtmlOptionsExpression'=>'array("data-id"=>$data["id"])',
										'summaryText' => 'แสดงข้อมูล: {start} - {end} จาก {count} รายการ',
										'pagerCssClass'=>'mailbox-pager',
										'pager' => array(
											'class'=>'CLinkPager',
											'header' => '',
											'firstPageLabel'=>'หน้าแรก',
											'prevPageLabel'=>'ก่อนหน้า',
											'nextPageLabel'=>'หน้าถัดไป', 
											'lastPageLabel'=>'หน้าสุดท้าย',	

										),		
										'columns' => array(
											/*
											array(
												'name'=>'id',
												'type'=>'raw',
												'header' => 'ลำดับ',
												'htmlOptions'=>array('style'=>'text-align:center;'),
												'headerHtmlOptions'=>array('style'=>'width:150px; text-align:center;'),
											  ),
											  */
											array(
												'name'=>'code',
												'header' => 'Username',
												'htmlOptions'=>array('style'=>'text-align:left;'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											  ),	
											array(
												'name'=>'name',
												'header' => 'ชื่อ',
												'htmlOptions'=>array('style'=>'text-align:left;'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											  ),	
											array(
												'name'=>'address',
												'header' => 'ที่อยู่',
												'htmlOptions'=>array('style'=>'text-align:center;'),
												'headerHtmlOptions'=>array('style'=>'width:500px;text-align:center;'),
											  ),
											array(
												'name'=>'tel',
												'header' => 'เบอร์โทร',
												'htmlOptions'=>array('style'=>'text-align:center;'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											  ),
											array(
												'name'=>'active',
												'header' => 'สถานะ',
												'htmlOptions'=>array('style'=>'text-align:center;'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											  ),
											 array(
												'header' => 'แก้ไข',
												'class' => 'CLinkColumn',
												'label' => '<i class="far fa-edit"></i>',
												'htmlOptions' => array(
													'width' => '35px',
													'align' => 'center',
													'onclick'=>'setUpdate(this);'
												),
												'linkHtmlOptions'=>array('class'=>'btn btn-info'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											), 	
											/*
											array(
												'header' => 'ลบ',
												'class' => 'CLinkColumn',
												'label' => '<i class="fas fa-trash-alt"></i>',
												//'urlExpression' => 'Yii::app()->createUrl("department/delete", array("id" => $data["id"]))',					
												'htmlOptions' => array(
													'width' => '35px',
													'align' => 'center',
													'onclick' => 'setDelete(this);'
												),
												'linkHtmlOptions'=>array('class'=>'btn btn-danger'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											),			
											*/
										),
									));
									?>
									</div>
								</div>
								<!-- Row end -->
							</div>
						</div>
						<!-- Bootstrap tab card end -->
					</div>
				</div>
			</div>
			<!-- Page body end -->
		</div>
	</div>
</div>
<!-- Large modal -->

<div id="modaledit" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modaldetailLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">		  
		  <div class="card">			
			<div class="card-block">
				<form class="form-material">
					<div class="form-group form-default form-static-label">
						<input type="text" class="form-control" placeholder="กรุณากรอก" id="txtcode" />
						<span class="form-bar"></span>
						<label class="float-label">Username</label>
					</div>
					<div class="form-group form-default form-static-label">
						<input type="text" class="form-control" placeholder="กรุณากรอก" id="txtpass" />
						<span class="form-bar"></span>
						<label class="float-label">Password</label>
					</div>
					<div class="form-group form-default form-static-label">
						<input type="text" class="form-control fill" id="txtname" />
						<span class="form-bar"></span>
						<label class="float-label">ชื่อ</label>
					</div>
					<div class="form-group form-default form-static-label">
						<input type="text" class="form-control fill" id="txtaddress" />
						<span class="form-bar"></span>
						<label class="float-label">ที่อยู่</label>
					</div>
					<div class="form-group form-default form-static-label">
						<input type="text" class="form-control fill" id="txttel" />
						<span class="form-bar"></span>
						<label class="float-label">เบอร์โทร</label>
					</div>
					<div class="form-group form-default form-static-label">
						<select id="drpstatus" class="form-control fill">
							<option value="1">ใช้งาน</option>
							<option value="2">ยกเลิกใช้งาน</option>
						</select>						
						<span class="form-bar"></span>
						<label class="float-label">สถานะ</label>
					</div>
					
					
				</form>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btnSave" class="btn btn-primary">Save</button>
		 <input type="hidden" id="hdfid" />
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">	
	$(document).ready(function(){
		//$("#list-grid").addClass("w-100");
		$(".user").addClass("active");
		 
		
		$('Button[id=btnAdd]').click(function () {
			$("#modaldetailLabel").html("บันทึกข้อมูล");
			$("#modaledit").modal('show');
		});
		$('#modaledit').on('show.bs.modal', function (e) {

		});
		$('#modaledit').on('hidden.bs.modal', function (e) {
				$('#hdfid').val("");
				$('#txtcode').val("");
				$('#txtpass').val("");
				$('#txtname').val("");
				$('#txtaddress').val("");
				$('#txttel').val("");
				$('#drpstatus').val("1");
		});
		
		$('Button[id=btnSave]').click(function () {	
			var id = $('#hdfid').val();
			var code = $('#txtcode').val();
			var pass = $('#txtpass').val();
			var name = $('#txtname').val();
			var address = $('#txtaddress').val();
			var tel = $('#txttel').val();
			var active = $('#drpstatus').val();
			
			
			if(code==""){
				alert("กรุณากรอก Username !");
				return;
			}
			
			if(pass==""){
				alert("กรุณากรอก Password !");
				return;
			}
			
			 $.ajax({
				type: "POST",
				url: "<?php echo Yii::app()->createAbsoluteUrl("user/savedata"); ?>",
				data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'code':code,'pass':pass,'name':name,'address':address,'tel':tel,'active':active },
				dataType: "json",				
				success: function (data) {
					if (data.status=='success') {
						$("#modaledit").modal('hide');
						//alert("ทำรายการสำเร็จ.");
						location.reload();
						//getSearch();
					}
					else{
						alert(data.msg);
					} 
				}
			});
		});
	});
	
	
	
	

	function setUpdate(el) {	
		var id = $(el).parent().attr("data-id"); 
		//alert(id);return;
		 $.ajax({
			type: "POST",
			url: "<?php echo Yii::app()->createAbsoluteUrl("user/setdata"); ?>",
			data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
			dataType: "json",				
			success: function (data) {
				if (data.status=='success') {
					$('#hdfid').val(data.id);
					$('#txtcode').val(data.code);
					$('#txtpass').val(data.pass);
					$('#txtname').val(data.name);
					$('#txtaddress').val(data.address);
					$('#txttel').val(data.tel);
					$('#drpstatus').val(data.active);					

					$("#modaldetailLabel").html("แก้ไขข้อมูล");   
					$("#modaledit").modal('show');  				

				}else{
					alert(data.msg);
				} 
			}
		});		

	}
	
</script>

