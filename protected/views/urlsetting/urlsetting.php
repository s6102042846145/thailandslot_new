<?php
	$this->pageTitle = 'Setting URL' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>
 <!-- Page-header start -->
<div class="page-header"style="height: 138px;">
	<div class="page-block">
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="page-header-title">
					<h5 class="m-b-10">Edit Link</h5>
					<p class="m-b-0">แก้ไข Url</p>
				</div>
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
	</div>
</div>
<!-- Page-header end -->
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<!-- Page body start -->
			<div class="page-body">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>URL</h5>
							</div>
							<div class="card-block">
								<form class="form-material">										
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtfacebook" />
										<span class="form-bar"></span>
										<label class="float-label">Facebook</label>
									</div>
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtline" />
										<span class="form-bar"></span>
										<label class="float-label">Line</label>
									</div>
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtlogin" />
										<span class="form-bar"></span>
										<label class="float-label">Login</label>
									</div>
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtregister" />
										<span class="form-bar"></span>
										<label class="float-label">Register</label>
									</div>
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtcontractus" />
										<span class="form-bar"></span>
										<label class="float-label">Contract Us</label>
									</div>
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtdownload" />
										<span class="form-bar"></span>
										<label class="float-label">Download</label>
									</div>
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtslot" />
										<span class="form-bar"></span>
										<label class="float-label">Slot Online</label>
									</div>
									<div class="form-group form-default form-static-label">
										<input type="text" name="footer-email" class="form-control" id="txtkunglo" />
										<span class="form-bar"></span>
										<label class="float-label">กงล้อพารวย</label>
									</div>
								</form>
							</div>
							
						</div>
					</div>
					<div class="col-md-12">
						<center>
							<button id="btnSave" class="btn btn-success waves-effect" data-type="success" data-from="top" data-align="right">บันทึก</button>
						</center>
					</div>
				</div>
			</div>
			<!-- Page body end -->
		</div>
	</div>
</div>
<!-- Large modal -->

<script type="text/javascript">	
	$(document).ready(function(){
		//$("#list-grid").addClass("w-100");
		$(".urlsetting").addClass("active");
		
		$("#txtfacebook").val('<?php echo Yii::app()->user->getInfo('url_facebook'); ?>');
		$("#txtline").val('<?php echo Yii::app()->user->getInfo('url_line'); ?>');
		$("#txtlogin").val('<?php echo Yii::app()->user->getInfo('url_login'); ?>');
		$("#txtregister").val('<?php echo Yii::app()->user->getInfo('url_register'); ?>');
		$("#txtcontractus").val('<?php echo Yii::app()->user->getInfo('url_contract'); ?>');
		$("#txtdownload").val('<?php echo Yii::app()->user->getInfo('url_download'); ?>');
		$("#txtslot").val('<?php echo Yii::app()->user->getInfo('url_slotonline'); ?>');
		$("#txtkunglo").val('<?php echo Yii::app()->user->getInfo('url_kunglo'); ?>');
		
		
		$('Button[id=btnSave]').click(function () {
			
			
			var facebook = $("#txtfacebook").val();
			var line = $("#txtline").val();
			var login = $("#txtlogin").val();
			var register = $("#txtregister").val();
			var contract = $("#txtcontractus").val();
			var download = $("#txtdownload").val();
			var slot = $("#txtslot").val();
			var kunglo = $("#txtkunglo").val();
			$.ajax({
				type: "POST",
				url: "<?php echo Yii::app()->createAbsoluteUrl("urlsetting/savedata"); ?>",
				data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','facebook':facebook,'line':line,'login':login,'register':register,
					   'contract':contract,'download':download,'slot':slot,'kunglo':kunglo
					  },
				dataType: "json",				
				success: function (data) {
					if (data.status=='success') {		
						alert("บันทึกข้อมูลสำเร็จ !")
					}
					else{
						alert(data.msg);
					} 
				}
			});
		});
	});

	
</script>

