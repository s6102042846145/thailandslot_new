<?php
	$this->pageTitle = 'กฎและกติกา' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>

                            <!-- Main-body start -->
                                <div class="main-body">
                                    <div class="page-wrapper">
                                        <!-- Page-body start -->
                                        <div class="page-body">
                                            <div class="row">
                                                <!-- Material statustic card start -->
                                                <div class="col-md-12">
                                                    <div class="card mat-stat-card">
                                                        <div class="card-block">  
                                                            <div class="text-center">
                                                                <img src="image/rules/topics.png" class="topic-page" />
                                                            </div>
                                                            <div class="text-center padding20">
                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว1.png" class="rule-img">
                                                                    <img id="t1" src="image/rules/ป้ายเงื่อนไขการสมัคร.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_01" src="image/rules/รายละเอียดเงื่อนไขการสมัคร.png" class="parag" style="display:none;">
                                                                
                                                                <div class="rule-menu">
                                                                    <img   src="image/rules/ป้ายหัว2.png" class="rule-img">
                                                                    <img id="t2" src="image/rules/ป้ายการฝากเงิน.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_02" src="image/rules/รายละเอียดการฝากเงิน.png" class="parag" style="display:none;">
                                                                
                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว2.png" class="rule-img">
                                                                    <img  id="t3" src="image/rules/ป้ายกฏการฝากทรูวอลเลท.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_03" src="image/rules/รายละเอียดกฏการฝากทรูวอลเลท.png" class="parag" style="display:none;">


                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว2.png" class="rule-img">
                                                                    <img  id="t4" src="image/rules/ป้ายการฝากเงินทศนิยม.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_04" src="image/rules/รายละเอียดการฝากเงินทศนิยม.png" class="parag" style="display:none;">
                                                                
                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว3.png" class="rule-img">
                                                                    <img  id="t5"src="image/rules/ป้ายการถอนเงิน.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_05" src="image/rules/รายละเอียดการถอนเงิน.png" class="parag" style="display:none;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Page-body end -->
                                    </div>
                                    <div id="styleSelector"> </div>
                                </div>
                                
<script type="text/javascript">
		$(document).ready(function(){
			$("#rules").addClass("active");

		});

        //เปิด slider terms of register
        $(document).ready(function(){
            $("#t1").click(function(){
                $("#terms_01").toggle();
            })

            $("#t2").click(function(){
                $("#terms_02").toggle();
            })

            $("#t3").click(function(){
                $("#terms_03").toggle();
            })

            $("#t4").click(function(){
                $("#terms_04").toggle();
            })

            $("#t5").click(function(){
                $("#terms_05").toggle();
            })
        })
</script>