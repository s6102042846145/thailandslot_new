<?php
	$this->pageTitle = 'Upload Images' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>
 <!-- Page-header start -->
<div class="page-header"style="height: 138px;">
	<div class="page-block">
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="page-header-title">
					<h5 class="m-b-10">Upload Images</h5>
					<p class="m-b-0">เพิ่ม ลบ แก้ไข รูปภาพ</p>
				</div>
			</div>
			<div class="col-md-4">
				<button type="button" id="btnAdd" class="btn btn-success float-right waves-effect waves-light">เพิ่มข้อมูล</button>
			</div>
		</div>
	</div>
</div>
<!-- Page-header end -->
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<!-- Page body start -->
			<div class="page-body">
				<div class="row">
					<div class="col-sm-12">
						<!-- Bootstrap tab card start -->
						<div class="card">
							<div class="card-block">
								<!-- Row start -->
								<div class="row">
									<div class="table-responsive">
									<?php
									
									$this->widget('zii.widgets.grid.CGridView', array(
										'id' => 'list-grid',
										'dataProvider' => $model,
										'itemsCssClass' => 'table table-bordered table-striped',	
										'rowHtmlOptionsExpression'=>'array("data-id"=>$data["id"])',
										'summaryText' => 'แสดงข้อมูล: {start} - {end} จาก {count} รายการ',
										'pagerCssClass'=>'mailbox-pager',
										'pager' => array(
											'class'=>'CLinkPager',
											'header' => '',
											'firstPageLabel'=>'หน้าแรก',
											'prevPageLabel'=>'ก่อนหน้า',
											'nextPageLabel'=>'หน้าถัดไป', 
											'lastPageLabel'=>'หน้าสุดท้าย',	

										),		
										'columns' => array(
											/*
											array(
												'name'=>'id',
												'type'=>'raw',
												'header' => 'รหัส',
												'htmlOptions'=>array('style'=>'text-align:center;'),
												'headerHtmlOptions'=>array('style'=>'width:150px; text-align:center;'),
											  ),*/
											array(
												'header' => 'รูปภาพ',	
												'type'=>'raw',
												'value'=>array($this, 'getData'),							
												'htmlOptions' => array(
													'width' => '100px',
													'align' => 'center',
													'style'=>'text-align:center;'
													//'onclick'=>'setReturn(this);'									
													),     

												'headerHtmlOptions'=>array('style'=>'width:60px;text-align:center;'),
											  ),
											
											array(
												'name'=>'name',
												'header' => 'ชื่อภาพ',
												'htmlOptions'=>array('style'=>'text-align:left;'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											  ),	
											array(
												'name'=>'grp',
												'header' => 'หมวดหมู่',
												'htmlOptions'=>array('style'=>'text-align:center;'),
												'headerHtmlOptions'=>array('style'=>'width:100px; text-align:center;'),
											  ),	
											 array(
												'header' => 'แก้ไข',
												'class' => 'CLinkColumn',
												'label' => '<i class="far fa-edit"></i>',
												'htmlOptions' => array(
													'width' => '35px',
													'align' => 'center',
													'onclick'=>'setUpdate(this);'
												),
												'linkHtmlOptions'=>array('class'=>'btn btn-info'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											), 	
											array(
												'header' => 'ลบ',
												'class' => 'CLinkColumn',
												'label' => '<i class="fas fa-trash-alt"></i>',
												//'urlExpression' => 'Yii::app()->createUrl("department/delete", array("id" => $data["id"]))',					
												'htmlOptions' => array(
													'width' => '35px',
													'align' => 'center',
													'onclick' => 'setDelete(this);'
												),
												'linkHtmlOptions'=>array('class'=>'btn btn-danger'),
												'headerHtmlOptions'=>array('style'=>'text-align:center;'),
											),			

										),
									));
									?>
									</div>
								</div>
								<!-- Row end -->
							</div>
						</div>
						<!-- Bootstrap tab card end -->
					</div>
				</div>
			</div>
			<!-- Page body end -->
		</div>
	</div>
</div>
<!-- Large modal -->

<div id="modaledit" class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modaldetailLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">		  
		<form method="post" action="" enctype="multipart/form-data" id="myform">
		  <div class="col-md-12">
			  <div class='preview'>
					<img src="" id="img" width="350" height="230">
				</div>
		  </div>
		  
		   <div class="col-md-12 ml-5 mt-2">
			   		<input type="file" id="file" name="file" />
			   <div class="form-group mt-2">
					<label>กลุ่ม</label>
					<select id="drpGrp">
					  <option value="">==กรุณาเลือก==</option>
					  <option value="slide">Slide</option>
					  <option value="promotion">Promotion</option>
					  <option value="bigwin">BigWin</option>
					  <option value="article">Article</option>
                      <option value="game">Games</option>
					  <option value="popup">Popup</option>
					</select>
				</div>
		  </div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btnSave" class="btn btn-primary">Save</button>
		  <input type="hidden" id="hdfImg" />
		  <input type="hidden" id="hdfUrl" />
		  <input type="hidden" id="hdfId" />
      </div>
    </div>
  </div>
</div>
<style>
.bd-example-modal-lg .modal-dialog{
    display: table;
    position: relative;
    margin: 0 auto;
    top: calc(50% - 24px);
  }
  
  .bd-example-modal-lg .modal-dialog .modal-content{
    background-color: transparent;
    border: none;
  }
</style>
<div id="modalload" class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width: 48px">
            <span class="fa fa-spinner fa-spin fa-3x"></span>
        </div>
    </div>
</div>

<script type="text/javascript">	
	$(document).ready(function(){
		//$("#list-grid").addClass("w-100");
		$(".uploadimages").addClass("active");
		 $("#file").change(function(){
			//$("#modalload").modal('show');
			var fd = new FormData();
			var files = $('#file')[0].files;
			//console.log(files[0].name);
			 $("#hdfImg").val(files[0].name);
			// Check file selected or not
			if(files.length > 0 ){
			   fd.append('file',files[0]);
			   $.ajax({
				  url: '<?php echo Yii::app()->request->baseUrl; ?>/protected/vendor/upload.php',
				  type: 'post',
				  data: fd,
				  headers: {
				  "accept": "application/json",
				  "Access-Control-Allow-Origin":"*"
			  },
				  crossDomain: true,
				  contentType: false,
				  processData: false,
				  success: function(response){
					 if(response != 0){
						$("#img").attr("src",response); 
						 $("#hdfUrl").val(response);
						$(".preview img").show(); // Display image element
						// $("#modalload").modal('hide');
					 }else{
						alert('file not uploaded');
					 }
				  },
			   });
			}else{
			   alert("Please select a file.");
			}
		});
		
		$('Button[id=btnAdd]').click(function () {
			$("#modaldetailLabel").html("บันทึกข้อมูล");
			$("#modaledit").modal('show');
		});
		$('#modaledit').on('show.bs.modal', function (e) {

		});
		$('#modaledit').on('hidden.bs.modal', function (e) {
				$('#hdfId').val("");
				$('#drpGrp').val("");
				$('#hdfImg').val("");
				
				$("#img").attr("src",""); 
				$("#hdfUrl").val("");
				$(".preview img").hide();
		});
		
		$('Button[id=btnSave]').click(function () {
			var id = $("#hdfId").val();
			var img = $("#hdfImg").val();
			var url = $("#hdfUrl").val();
			if(url==""){
				alert("กรุณาเลือกไฟล์ !");
				return;
			}
			var grp = $("#drpGrp").val();
			if(grp==""){
				alert("กรุณาเลือกกลุ่ม !");
				return;
			}
			 $.ajax({
				type: "POST",
				url: "<?php echo Yii::app()->createAbsoluteUrl("uploadimages/savedata"); ?>",
				data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'img':img,'url':url,'grp':grp },
				dataType: "json",				
				success: function (data) {
					if (data.status=='success') {
						$("#modaledit").modal('hide');
						//alert("ทำรายการสำเร็จ.");
						location.reload();
						
					}
					else{
						alert(data.msg);
					} 
				}
			});
		});
	});
function setDelete(el) {
	var id = $(el).parent().attr("data-id");    
	var r = confirm("คุณต้องการลบข้อมูลนี้ี้ใช่หรือไม่ !");
	
    if (r == true) {
	
	$.ajax({
    type: "POST",
    url: "<?php echo Yii::app()->createAbsoluteUrl("uploadimages/deletedata"); ?>",
  	data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
    dataType: "json",				
    success: function (data) {
	if (data.status=='success') {		
		$("#modaledit").modal('hide');
		//alert("ทำรายการสำเร็จ.");
		location.reload();
		
	}
	else{
		alert(data.msg);
	} 
	}
	});
  }
}	
function setUpdate(el) {	
	var id = $(el).parent().attr("data-id"); 
	//alert(id);return;
	 $.ajax({
		type: "POST",
		url: "<?php echo Yii::app()->createAbsoluteUrl("uploadimages/setdata"); ?>",
		data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
		dataType: "json",				
		success: function (data) {
			if (data.status=='success') {
				$('#hdfId').val(data.id);
				$('#drpGrp').val(data.grp);
				$('#hdfImg').val(data.name);
				
				$("#img").attr("src",data.url); 
				$("#hdfUrl").val(data.url);
				$(".preview img").show();
				
				$("#modaldetailLabel").html("แก้ไขข้อมูล");   
				$("#modaledit").modal('show');  				

			}else{
				alert(data.msg);
			} 
		}
	});		
	
}
	
</script>

