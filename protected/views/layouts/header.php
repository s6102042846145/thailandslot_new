<!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="desktop navbar header-navbar pcoded-header">
                <div class="navbar-top" style="background-image:url(image/แถบล่าง.png);margin-left: -1px;">
                    <div class="left-bar">
						<?php
							$data=lookupdata::getURL();																			
							foreach($data as $dataitem) { 
								echo '	<a  href="'.$dataitem['url_line'].'"><img src="image/line icon.png" style="width:45px; margin-bottom: 2px;"></a>
										<a  href="'.$dataitem['url_facebook'].'"><img src="image/face icon.png" style="width:50px;"></a>
										<a class="menubar-a-left" href="">@ThailandSlot</a>
										<a class="menubar-a-left-last" href="'.$dataitem['url_slotonline'].'">สล็อตออนไลน์</a>								
									';
							} 
						?>
                    </div>

                    <div class="right-bar">
						<?php
							$data=lookupdata::getURL();																			
							foreach($data as $dataitem) { 
								echo '	<a href="'.$dataitem['url_download'].'" class="menubar-a">ดาวน์โหลด</a>
										<a href="promotion" class="menubar-a" href="">โปรโมชั่น</a>
										<a href="rules" class="menubar-a" href="">กฎและกติกา</a>
										<a href="'.$dataitem['url_contract'].'" class="menubar-a-last" href="">ติดต่อเรา</a>
								
									';
							} 
						?>
                    </div>
                </div>

                <div class="header-navbar pcoded-header" style="margin-top: -2px;">
                    <div class="menu-top sp-desktop"> 
                        <ul class="nav-left">
                            <li>
                                <a class="mobile-menu waves-effect waves-light"  href="/home" data-aos="zoom-out-left">
                                    <img class="hovOpa slot-logo" src="image/Top/logothslot.png" />
                                </a>
                            </li>
                        </ul>

                        <div class="nav-center">
                            <img id="logo-responsive" src="image/logo.png" class="img-logo">
                        </div>

                        <ul class="nav-right">
                            <li class="">
                                <a href="login" class="waves-effect waves-light" data-aos="zoom-out-right">
                                    <img class="hovOpa top-bar" src="image/ป้ายเข้าสู่ระบบ.png" />
                                </a>
                            </li>
                            <li class="">
                                <a href="register" class="waves-effect waves-light"  data-aos="zoom-out-right">
                                    <img class="hovOpa top-bar" src="image/ป้าย สมัครสมาชิก.png" />
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="navbar-wrapper bg-mt">
                        <div class="navbar-logo">
			                <a class="mobile-show waves-effect waves-light" id="mobile-collapse" style="position: absolute;z-index: 999;left: 0;">
                                <div id="addClass" class="burger-none">&nbsp</div>
                            </a>
                                                            
                            <script>
                                    $(document).ready(function(){
                                        $("#mobile-collapse").click(function(){
                                            $("#addClass").toggleClass("burger-closed");
                                        });
                                    });

                                    
                            </script>
                            <div class="mobile">
                                <img  src="image/logo.png" class="img-logo">
                            </div>
							<?php
								$data=lookupdata::getURL();																			
								foreach($data as $dataitem) { 
									echo '	<a href="'.$dataitem['url_facebook'].'" class="mobile-options waves-effect waves-light" style="margin-right: 45px;">
												<img class="hovOpa" src="image/face icon.png" style="width: 50px;" />
											</a>
											<a href="'.$dataitem['url_line'].'" class="mobile-options waves-effect waves-light">
												<img class="hovOpa" src="image/line icon.png" style="width: 45px;" />
											</a>								
										';
								} 
							?>
                        </div>
                            

                    </div>
                </div>

                <!-- Navbar new -->
                <div class="navbar-bottom" >
                    <ul id="ul-responsive" class="menu-bottom" style="margin-top: 100px;">
                        <li class="menu-bar">
                            <a class="menu-bar" href="home">
                                <img class="hovOpa" src="image/ป้าย หน้าหลัก.png" alt="">
                            </a>
                        </li>
                        <li class="menu-bar">
                            <a  class="menu-bar"href="promotion">
                                <img class="hovOpa" src="image/Top/ป้าย โปรโมชั่น.png " alt="">
                            </a>
                        </li>
                        <li class="menu-bar">
                            <a  class="menu-bar"href="rules">
                                <img class="hovOpa rules" src="image/Top/rules.png" alt="">

                            </a>
                        </li>
                        <li  class="menu-bar">
							<?php
								$data=lookupdata::getURL();																			
								foreach($data as $dataitem) { 
									echo '	 <a class="menu-bar" href="'.$dataitem['url_contract'].'">
												<img class="hovOpa" src="image/Top/contact.png">

											</a>							
										';
								} 
							?>
                           
                        </li>
                    </ul>
                </div>
                <!-- end navbar new -->
            </nav>
	   
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
		            <nav class="nav-none pcoded-navbar">
                        <div class="sidebar_toggle">
                            <a href="#">
                                <i class="icon-close icons"></i>
                            </a>
                        </div>
                        <div class="pcoded-inner-navbar main-menu" style="background-image:url('image/bg_menu.png')">
                            <ul class="pcoded-item pcoded-left-item">
								<li id="home" class="mt-5">
                                    <a href="home">หน้าแรก</a>
                                </li>
								<li id="register" class="">
                                    <a href="register">สมัครสมาชิก</a>
                                </li>
								<li id="login" class="">
                                    <a href="login">เข้าสู่ระบบ</a>
                                </li>
								<li id="rules" class="">
                                    <a href="rules" style="display: flex;">กฏ - กติกา <div class="warn"><span>อ่าน</span></div></a>
                                </li>
								<!--li id="deposit" class="">
                                    <a href="">ฝาก - ถอน</a>
                                </li-->
								<?php
									$data=lookupdata::getURL();																			
									foreach($data as $dataitem) { 
										echo '	<li id="download" class="">
													<a href="'.$dataitem['url_download'].'">ดาวน์โหลด</a>
												</li>							
											';
									} 
								?>
								
								<li id="promotion" class="">
                                    <a href="promotion">โปรโมชั่น</a>
                                </li>
							</ul>                          
                        </div>
                    </nav>	
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <!-- navbar-new  mobile-->
                            <div class="navbar-new mobile">
                                <ul class="mt-4 nav-bar">
                                    <li class="menu-bar">
                                        <a href="rules" class="hovOpa">
                                            <img class="hovOpa rules Mobi" src="image/Top/rules.png">

                                        </a>
                                    </li>
                                    <li class="menu-bar">
										<?php
											$data=lookupdata::getURL();																			
											foreach($data as $dataitem) { 
												echo '	 <a class="hovOpa" href="'.$dataitem['url_kunglo'].'" >
															<img class="hovOpa Mobi" src="image/Top/ป้าย กงล้อพารวย.png">
														</a>							
													';
											} 
										?>                                       
                                    </li>
                                </ul>
                            </div>