<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
		<meta name="author" content="Codedthemes" />
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
		<!-- waves.css -->
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/bootstrap/css/bootstrap.min.css">
		<!-- waves.css -->
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
		<!-- themify icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/icon/themify-icons/themify-icons.css">
		<!-- font-awesome-n -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/font-awesome-n.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/font-awesome.min.css">
		<!-- scrollbar.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/jquery.mCustomScrollbar.css">
		<link rel="stylesheet" type="text/javascript" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/burger.js">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/app.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/rules.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/game.css">
		
		<!-- slider game page using bootstrap-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- font thai -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">

        <!-- aos animate -->
        <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css" />
		
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>    
	</head>
    <script>
        $(document).ready(function() {
        $(window).on("scroll", function() {
            if ($(window).scrollTop() >= 20) {
            $(".navbar-top").addClass("compressed");
            $("#change-responsive").addClass("responsive-bg");
            $("#change-responsive").removeClass("bg");
            $("#logo-responsive").addClass("img-logo-2");
            $("#ul-responsive").addClass("ul-mt");
            } else {
            $(".navbar-top").removeClass("compressed");
            $("#change-responsive").removeClass("responsive-bg");
            $("#change-responsive").addClass("bg");
            $("#logo-responsive").removeClass("img-logo-2");
            $("#ul-responsive").removeClass("ul-mt");
            }
        });
        });
    </script>
	<body>
	<!-- Pre-loader start -->
    
	
                            <!-- end navbar new -->	
								<?php echo $content; ?> 
							
							

    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery/jquery.min.js "></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery-ui/jquery-ui.min.js "></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/popper.js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/bootstrap/js/bootstrap.min.js "></script>
    <!-- waves js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/pages/waves/js/waves.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- slimscroll js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.mCustomScrollbar.concat.min.js "></script>

    <!-- menu js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/pcoded.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/vertical/vertical-layout.min.js "></script>

    <!-- js aos animate -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/script.js "></script>
	
    <script type="text/javascript">
		$(document).ready(function(){
			$(".fixed-button").addClass("d-none");
			$(".login").click(function(){
			  window.location.assign("/login");
			});
			$(".register").click(function(){
			  window.location.assign("/register");
			});
			$(".facebook").click(function(){
			  window.location.assign("https://www.facebook.com/");
			});
		});

        //close tab
        $("#close").click(function(){
            $(".fixed-left").hide();
        })
        
        //animation css
        AOS.init({
            easing: 'ease-out-back',
            duration: 1000
        });
		
	</script>
</body>

</html>



       



