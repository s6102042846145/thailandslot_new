<?php

class frm_home extends CFormModel
{
	public $id;
	public $name;
	
	/*
	public function rules()
	{
		return array(
			array('id','name'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	
	*/

	public function save_insert()
	{
		$sql = "insert into mas_member (name,create_date,create_by) ";
		$sql.= "values (:name,now(),1) ";
		
		$command=yii::app()->db->createCommand($sql);
		$command->bindValue(":name", $this->name);	
		if($command->execute()) {			
			//$id = Yii::app()->db->getLastInsertID();
				
			return true;
		} else { 
			Yii::app()->session['errmsg_home']='บันทึกไม่สำเร็จ';
			return false;
		}	
	}
		
}
