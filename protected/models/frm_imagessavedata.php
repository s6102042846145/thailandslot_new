<?php

class frm_imagessavedata extends CFormModel
{
	public $id;
	public $img;
	public $url;
	public $grp;
	
	
	public function rules()
	{
		return array(
			array('id','img', 'url','grp'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	public function save_insert()
	{
		$sql = "insert into slot_mas_images (name,url,grp,create_date,create_by) ";
		$sql.= "values (:name,:url,:grp,now(),1) ";
		
		$command=yii::app()->db->createCommand($sql);
		$command->bindValue(":name", $this->img);	
		$command->bindValue(":url", $this->url);	
		$command->bindValue(":grp", $this->grp);	
		if($command->execute()) {			
			///$id = Yii::app()->db->getLastInsertID();				
			return true;
		} else { 
			Yii::app()->session['errmsg_image']='บันทึกไม่สำเร็จ';
			return false;
		}	
	}
	public function save_update()
	{
		
			$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		
			$sql = "update slot_mas_images set name=:name,url=:url,grp=:grp, ";
			$sql.= "update_date=now(), update_by=$updateby where id='".$this->id."'";
			$command=yii::app()->db->createCommand($sql);			
			$command->bindValue(":name", $this->img);	
			$command->bindValue(":url", $this->url);	
			$command->bindValue(":grp", $this->grp);
				if($command->execute()) {
					return true;
				} else {
					Yii::app()->session['errmsg_image']='ไม่สามารถบันทึกข้อมูลได้'.$sql;
					return false;
			}	
	}
	public function save_delete()
	{
		
			$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
			$sql = "update slot_mas_images set status=0, update_date=now(), update_by=$updateby where id='".$this->id."'";
			$command=yii::app()->db->createCommand($sql);			
				if($command->execute()) {
					return true;
				} else {
					Yii::app()->session['errmsg_image']='ไม่สามารถลบข้อมูลได้'.$sql;
					return false;
			}	
	}
		
}
