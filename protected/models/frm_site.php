<?php

class frm_assetsavedata extends CFormModel
{
	public $id;
	public $idspec;
	public $barcode;	
	public $assetstatus;
	public $acquiry;		
	public $purchase;
	public $contract;
	public $barcodefor;
	public $usedtype;	
	public $assetno;
	public $serial;
	public $assetcode;		
	public $department;
	public $departmentg;
	public $location;
	public $owner;	
	public $remark;
	public $contractma;
	public $registerdate;
	public $totalyear;
	public $unitprice;
	public $totalappyear;	
	public $curunitprice;
	public $status;
	
	
	
	
		
	public function rules()
	{
		return array(
			array('code', 'id','idspec','barcode','assetstatus','acquiry','purchase','contract','barcodefor',
				  'usedtype','assetno','serial','assetcode','department','departmentg','location',
				  'owner','remark','contractma','registerdate','totalyear','unitprice','totalappyear','curunitprice',
				  'status','safe'
				  ),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	
	

	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
		/*
		$sql ="select count(*) as aa from mas_assetspec where status=1 and name='".$this->name."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg_user']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}		
		*/
		//save
		$gen_barcode= Yii::app()->CommonFnc->genstring(10);
		Yii::app()->session['gen_barcode']=$gen_barcode;
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		
		
		$sql = "INSERT INTO mas_asset ";
		$sql.= "(barcode,status_id,acquiry_method_id,purchase_method_id,po_contract_id, ";
		$sql.= "replace_for_asset_id,used_type,asset_no,serial_no,asset_code,department_id,departmentgroup_id, ";
		$sql.= "ma_contract_id,location,owner,remark,assetspec_id,register_date,totalyear,unitprice,totalappyear, ";
		$sql.= "curunitprice,create_date,create_by) ";
		$sql.= "VALUES('".$gen_barcode."',:assetstatus,:acquiry,:purchase,:contract, ";
		$sql.= ":barcodefor,:usedtype,:assetno,:serial,:assetcode,:department,:departmentg, ";
		$sql.= ":contractma,:location,:owner,:remark,:idspec,:registerdate,:totalyear,:unitprice, ";
		$sql.= ":totalappyear,:curunitprice,now(),$createby) ";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":assetstatus", $this->assetstatus);	
		$command->bindValue(":acquiry", $this->acquiry);	
		$command->bindValue(":purchase", $this->purchase);
		$command->bindValue(":contract", $this->contract);
		$command->bindValue(":barcodefor", $this->barcodefor);	
		$command->bindValue(":usedtype", $this->usedtype);	
		$command->bindValue(":assetno", $this->assetno);	
		$command->bindValue(":serial", $this->serial);	
		$command->bindValue(":assetcode", $this->assetcode);
		$command->bindValue(":department", $this->department);
		$command->bindValue(":departmentg", $this->departmentg);
		$command->bindValue(":location", $this->location);
		$command->bindValue(":owner", $this->owner);
		$command->bindValue(":remark", $this->remark);	
		$command->bindValue(":idspec", $this->idspec);	
		$command->bindValue(":registerdate", $this->registerdate);
		$command->bindValue(":totalyear", $this->totalyear);
		$command->bindValue(":unitprice", $this->unitprice);
		$command->bindValue(":totalappyear", $this->totalappyear);
		$command->bindValue(":curunitprice", $this->curunitprice);
		$command->bindValue(":contractma", $this->contractma);			
		
			
		if($command->execute()) {			
			$id = Yii::app()->db->getLastInsertID();
				
			return true;
		} else { 
			Yii::app()->session['errmsg_user']='เกิดข้อผิดพลาดบันทึก  ไม่สำเร็จ';
			return false;
		}			
	}	

	public function save_update()
	{
			
		/*	$sql ="select count(*) as aa from mas_assetspec where status=1 and name='".$this->name."' and id!='".$this->id."'";
			$data =Yii::app()->db->createCommand($sql)->queryAll();
			foreach($data as $dataitem){
				if ($dataitem['aa']>0){
					Yii::app()->session['errmsg_user']='มีข้อมูลนี้ในระบบแล้ว';
					return false;
					}
				}	*/	
		
		//save
		
			$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		
			$sql = "update mas_asset set  ";
			$sql.= "barcode=:barcode,status_id=:assetstatus,acquiry_method_id=:acquiry,purchase_method_id=:purchase,";
			$sql.= "po_contract_id=:contract,replace_for_asset_id=:barcodefor,used_type=:usedtype, ";
			$sql.= "asset_no=:assetno,serial_no=:serial,asset_code=:assetcode,department_id=:department,";
			$sql.= "departmentgroup_id=:departmentg,ma_contract_id=:contractma,location=:location,owner=:owner, ";
			$sql.= "remark=:remark,assetspec_id=:idspec,register_date=:registerdate,totalyear=:totalyear,";
			$sql.= "unitprice=:unitprice,totalappyear=:totalappyear,curunitprice=:curunitprice,";
			$sql.= "update_date=now(),update_by=$updateby where id='".$this->id."'";
			$command=yii::app()->db->createCommand($sql);			
			$command->bindValue(":barcode", $this->barcode);	
			$command->bindValue(":assetstatus", $this->assetstatus);	
			$command->bindValue(":acquiry", $this->acquiry);	
			$command->bindValue(":purchase", $this->purchase);
			$command->bindValue(":contract", $this->contract);
			$command->bindValue(":barcodefor", $this->barcodefor);	
			$command->bindValue(":usedtype", $this->usedtype);	
			$command->bindValue(":assetno", $this->assetno);	
			$command->bindValue(":serial", $this->serial);	
			$command->bindValue(":assetcode", $this->assetcode);
			$command->bindValue(":department", $this->department);
			$command->bindValue(":departmentg", $this->departmentg);
			$command->bindValue(":location", $this->location);
			$command->bindValue(":owner", $this->owner);
			$command->bindValue(":remark", $this->remark);	
			$command->bindValue(":idspec", $this->idspec);	
			$command->bindValue(":registerdate", $this->registerdate);
			$command->bindValue(":totalyear", $this->totalyear);
			$command->bindValue(":unitprice", $this->unitprice);
			$command->bindValue(":totalappyear", $this->totalappyear);
			$command->bindValue(":curunitprice", $this->curunitprice);
			$command->bindValue(":contractma", $this->contractma);				
				if($command->execute()) {			
					$id = Yii::app()->db->getLastInsertID();						
					return true;
				} else { 
					Yii::app()->session['errmsg_user']='เกิดข้อผิดพลาดบันทึก  ไม่สำเร็จ';
					return false;
				}			
	}
	public function save_spec()
	{
		
		$gen_code= Yii::app()->CommonFnc->genstring(10);
		Yii::app()->session['assetspac_gen_code']=$gen_code;
		

		$sql = " insert into mas_assetspecitm_temp (gen_code,assetspec_id,assetspecitm_id,name) ";		
		$sql.= " select '".$gen_code."', templatespec_id,id,name ";
		$sql.= " from mas_templatespecitm ";						
		$command=yii::app()->db->createCommand($sql);	
		if($command->execute()) {
			$id = Yii::app()->db->getLastInsertID();		
			return true;
		} else { 
			Yii::app()->session['errmsg_user']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}
	
	
public function save_detailadd()
	{		
		$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql = "update mas_assetspecitm_temp set value=:value where name='".$this->name."'";
		$command=yii::app()->db->createCommand($sql);			
		$command->bindValue(":value", $this->value);								
			if($command->execute()) {
				return true;
			} else {
				Yii::app()->session['errmsg_user']='ไม่สามารถบันทึกข้อมูลได้'.$sql;
				return false;
			}	
	}
	
public function save_detailedit()
	{		
		$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql = "update mas_assetspecitm set value=:value where name='".$this->name."'";
		$command=yii::app()->db->createCommand($sql);			
		$command->bindValue(":value", $this->value);								
			if($command->execute()) {
				return true;
			} else {
				Yii::app()->session['errmsg_user']='ไม่สามารถบันทึกข้อมูลได้'.$sql;
				return false;
			}	
	}
	
	
	public function save_delete()
	{
		
			$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
			$sql = "update mas_assetspec set status=0, update_date=now(), update_by=$updateby where id='".$this->id."'";
			$command=yii::app()->db->createCommand($sql);			
				if($command->execute()) {
					return true;
				} else {
					Yii::app()->session['errmsg_sample']='ไม่สามารถลบข้อมูลได้'.$sql;
					return false;
			}	
	}
		
}
