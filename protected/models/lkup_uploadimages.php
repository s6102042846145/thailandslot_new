<?php


class lkup_uploadimages extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'slot_mas_images';
	}

    public function attributeLabels() {
        return array(
        );
    }
	
    public function search($keyword=null) {
		
		$sqlCon="";
		
			if($keyword!=''){							
				$sqlCon.= " and (name like '%".$keyword."%' ";	
				$sqlCon.= " or id like '%".$keyword."%') ";				
			}
			
		
		$count=Yii::app()->db->createCommand('select count(*) from slot_mas_images where status=1'.$sqlCon)->queryScalar();
		$sql="select * from slot_mas_images where status=1 order by id desc";
		
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'id','name','desc','url','pic',
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
	
	
	public function getData($id = null)
	{
	   $sql="select * from slot_mas_images where id='".$id."'";	   
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	

}		