<?php


class lkup_user extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'slot_mas_user';
	}

    public function attributeLabels() {
        return array(
        );
    }
	
    public function search($keyword=null) {
		
		$sqlCon="";
		
			if($keyword!=''){							
				$sqlCon.= " and (name like '%".$keyword."%' ";	
				$sqlCon.= " or id like '%".$keyword."%') ";				
			}
			
		
		$count=Yii::app()->db->createCommand('select count(*) from slot_mas_user where status!=0'.$sqlCon)->queryScalar();
		$sql="select *,case when status=1 then 'ใช้งานอยู่' else 'ยกเลิกใช้งาน' end active from slot_mas_user where status!=0 order by id desc";
		
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'id','code','name','email','address','tel',
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
	
	
	public function getData($id = null)
	{
	   $sql="select * from slot_mas_user where id='".$id."' ";			
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	

}		