<?php

class frm_usersavedata extends CFormModel
{
	public $id;
	public $code;
	public $pass;
	public $name;
	public $address;
	public $tel;
	public $active;
	
	public function rules()
	{
		return array(
			array('id','code', 'pass','name','address','tel', 'active',),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
		
		$sql ="select count(*) as aa from slot_mas_user where status=1 and code='".$this->code."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg_user']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}
		
		//echo var_dump($this->status);exit();
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		
		$sql = "INSERT INTO slot_mas_user ( code, pass, name, address,tel, status, create_date, create_by) ";
		$sql.= "VALUES(:code, :pass, :name, :address, :tel, :active, now(), $createby)";
		$command=yii::app()->db->createCommand($sql);
		$command->bindValue(":code", $this->code);
		$command->bindValue(":pass", $this->pass);
		$command->bindValue(":name", $this->name);
		$command->bindValue(":address", $this->address);
		$command->bindValue(":tel", $this->tel);
		$command->bindValue(":active", $this->active);
		
		
		if($command->execute()) {
			$id = Yii::app()->db->getLastInsertID();			
			return true;
		} else { 
			Yii::app()->session['errmsg_user']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}
	
	
	
	public function save_update()
	{
		
			$sql ="select count(*) as aa from slot_mas_user where status=1 and code='".$this->code."' and id !='".$this->id."' ";
			$data =Yii::app()->db->createCommand($sql)->queryAll();
			foreach($data as $dataitem){
				if ($dataitem['aa']>0){
					Yii::app()->session['errmsg_user']='มีข้อมูลนี้ในระบบแล้ว';
					return false;
					}
				}
			//echo var_dump($this->status);exit;
			$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;				
		
			$sql = "update slot_mas_user set code=:code,pass=:pass,name=:name, address=:address,tel=:tel,status=:active, ";
			$sql.= "update_date=now(), update_by=$updateby ";
			$sql.= "where id='".$this->id."'";
			$command=yii::app()->db->createCommand($sql);			
			$command->bindValue(":code", $this->code);
			$command->bindValue(":pass", $this->pass);
			$command->bindValue(":name", $this->name);
			$command->bindValue(":address", $this->address);
			$command->bindValue(":tel", $this->tel);
			$command->bindValue(":active", $this->active);
				if($command->execute()) {
					return true;
				} else {
					Yii::app()->session['errmsg_user']='ไม่สามารถบันทึกข้อมูลได้'.$sql;
					return false;
			}	
	}
		
}
