<?php

class frm_urlsettingsavedata extends CFormModel
{
	public $facebook;
	public $line;
	public $login;
	public $register;
	public $contract;
	public $download;
	public $slot;
	public $kunglo;
	
	
	public function rules()
	{
		return array(
			array('id','img', 'url','grp'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	
	public function save_update()
	{
		
			$updateby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		
			$sql = "update slot_mas_user set url_facebook=:facebook,url_line=:line,url_login=:login, url_register=:register,url_contract=:contract,url_download=:download, url_slotonline=:slot, ";
			$sql.= "url_kunglo=:kunglo, update_date=now(), update_by=$updateby ";
			$command=yii::app()->db->createCommand($sql);			
			$command->bindValue(":facebook", $this->facebook);	
			$command->bindValue(":line", $this->line);	
			$command->bindValue(":login", $this->login);
			$command->bindValue(":register", $this->register);	
			$command->bindValue(":contract", $this->contract);	
			$command->bindValue(":download", $this->download);
			$command->bindValue(":slot", $this->slot);
			$command->bindValue(":kunglo", $this->kunglo);
			if($command->execute()) {
				return true;
			} else {
				Yii::app()->session['errmsg_url']='ไม่สามารถบันทึกข้อมูลได้'.$sql;
				return false;
			}	
	}
		
}
