<?php

class UrlSettingController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{
		
		$this->layout='main_admin';	
		$this->render('urlsetting');	
		
	}
		
	public function actionSavedata()
	{
			   
		$model=new frm_urlsettingsavedata;		
		
		$model->facebook=isset($_POST['facebook'])?addslashes(trim($_POST['facebook'])):'';
		$model->line=isset($_POST['line'])?addslashes(trim($_POST['line'])):'';
		$model->login=isset($_POST['login'])?addslashes(trim($_POST['login'])):'';
		$model->register=isset($_POST['register'])?addslashes(trim($_POST['register'])):'';
		$model->contract=isset($_POST['contract'])?addslashes(trim($_POST['contract'])):'';
		$model->download=isset($_POST['download'])?addslashes(trim($_POST['download'])):'';
		$model->slot=isset($_POST['slot'])?addslashes(trim($_POST['slot'])):'';
		$model->kunglo=isset($_POST['kunglo'])?addslashes(trim($_POST['kunglo'])):'';
	
		
		if($model->save_update()) {
			echo CJSON::encode(array('status' => 'success','msg' => '',));		 
		} else {
			echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_setting'], ));		
			Yii::app()->session->remove('errmsg_setting');	
		}	
		
	}
	
}