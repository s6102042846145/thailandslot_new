<?php

class UserController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{
		
		$this->layout='main_admin';	
		$model = lkup_user::search();
		$this->render('user', array('model' => $model));	
		
	}
	public function getData($data) 
	{		
		//echo var_dump($data);exit;
		$ret = "<img src='".$data['url']."' style='width:100px;' />";
		return $ret; 
		
	}	
	public function actionSearch()
	{
		$this->layout='main_admin';
		$model = lkup_user::search();			
		$this->renderPartial('user', array('model'=>$model));
		
		
	}
	public function actionSetdata(){
		
		
		$code="";
		$pass="";
		$name="";
		$address="";
		$tel="";
		$status="";
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_user::getData($id);
		//echo var_dump($data);exit;
		foreach($data as $dataitem){			
			$id=$dataitem['id'];
			$code=$dataitem['code'];
			$pass=$dataitem['pass'];
			$name=$dataitem['name'];
			$address=$dataitem['address'];
			$tel=$dataitem['tel'];
			$status=$dataitem['status'];
		}
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'code'=>$code,
			'pass'=>$pass,
			'name'=>$name,
			'address'=>$address,
			'tel'=>$tel,
			'active'=>$status,
			));		

	}	
	public function actionSavedata()
	{
		//'id':id,'code':code,'pass':pass,'name':name,'address':address,'tel':tel,'status':status 
		$model=new frm_usersavedata;		
		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->code=isset($_POST['code'])?addslashes(trim($_POST['code'])):'';
		$model->pass=isset($_POST['pass'])?addslashes(trim($_POST['pass'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
		$model->address=isset($_POST['address'])?addslashes(trim($_POST['address'])):'';
		$model->tel=isset($_POST['tel'])?addslashes(trim($_POST['tel'])):'';
		$model->active=intval($_POST['active']);
	//echo var_dump($model->active);exit;
		if($model->id==''){
			if($model->save_insert()) {
				echo CJSON::encode(array('status' => 'success','msg' => '',));		 
			} else {
				echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_user'], ));		
				Yii::app()->session->remove('errmsg_user');
			}
		}else{
			if($model->save_update()) {
				echo CJSON::encode(array('status' => 'success','msg' => '',));		 
			} else {
				echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_user'], ));		
				Yii::app()->session->remove('errmsg_user');	
			}	
		}
	}
	
	
	
	
	
	
	public function actionDeletedata(){

		$model=new frm_imagessavedata;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';		
	
	
			if($model->save_delete()) {
					echo CJSON::encode(array('status' => 'success','msg' => '',));		 
				} else {
					echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_image'], ));		
						Yii::app()->session->remove('errmsg_image');
				}
		
		}
}