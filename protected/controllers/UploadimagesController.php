<?php

class UploadimagesController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{
		
		$this->layout='main_admin';	
		$model = lkup_uploadimages::search();
		$this->render('uploadimages', array('model' => $model));	
		
	}
	public function getData($data) 
	{		
		//echo var_dump($data);exit;
		$ret = "<img src='".$data['url']."' style='width:100px;' />";
		return $ret; 
		
	}	
	public function actionSearch()
	{
		$this->layout='main_admin';
		$model = lkup_uploadimages::search();			
		$this->render('uploadimages', array('model'=>$model));
		
		
	}
	public function actionSetdata(){
		
		$name="";
		$url="";
		$grp="";
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_uploadimages::getData($id);
		foreach($data as $dataitem){
			
			$id=$dataitem['id'];
			$url=$dataitem['url'];
			$name=$dataitem['name'];
			$grp=$dataitem['grp'];
			
		
		}
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'name'=>$name,
			'url'=>$url,
			'grp'=>$grp,
			));		

	}	
		
		
		
		
	public function actionSavedata(){

		$model=new frm_imagessavedata;		
		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->img=isset($_POST['img'])?addslashes(trim($_POST['img'])):'';
		$model->url=isset($_POST['url'])?addslashes(trim($_POST['url'])):'';
		$model->grp=isset($_POST['grp'])?addslashes(trim($_POST['grp'])):'';
	
		if($model->id==''){
			if($model->save_insert()) {
				echo CJSON::encode(array('status' => 'success','msg' => '',));		 
			} else {
				echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_image'], ));		
				Yii::app()->session->remove('errmsg_image');
			}
		}else{
			if($model->save_update()) {
				echo CJSON::encode(array('status' => 'success','msg' => '',));		 
			} else {
				echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_image'], ));		
				Yii::app()->session->remove('errmsg_image');	
			}	
		}
	}
	public function actionDeletedata(){

		$model=new frm_imagessavedata;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';		
	
	
			if($model->save_delete()) {
					echo CJSON::encode(array('status' => 'success','msg' => '',));		 
				} else {
					echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_image'], ));		
						Yii::app()->session->remove('errmsg_image');
				}
		
		}
}