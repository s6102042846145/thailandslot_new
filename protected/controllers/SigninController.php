<?php

class SigninController extends Controller
{
	public function actionIndex()
	{
		$this->layout='main_login';
		if(!Yii::app()->user->isGuest) { 
			//$this->redirect(Yii::app()->getBaseUrl(true)); 
			$this->redirect(Yii::app()->createUrl('uploadimages'));
		}
		$this->render('signin');		
		
	}
	public function actionAuth()
	{
		$model=new frm_login;
		$model->username = isset($_POST['txtusername'])?addslashes(trim($_POST['txtusername'])):'';
		$model->password = isset($_POST['txtpassword'])?addslashes(trim($_POST['txtpassword'])):'';

		if($model->login()) {
			//echo var_dump(1);exit;
			//$this->redirect('../user');
			$user = Yii::app()->user->getInfo('id');	
			Yii::app()->CommonFnc->log_login('Login','success',$user);
			echo CJSON::encode(array('status' => 'success','msg' => '',));		 
		} else {
			//echo var_dump(22);exit;
			//$this->redirect('product');
			//Yii::app()->CommonFnc->log_login('Error',Yii::app()->session['errmsg_login2'],$user);
			echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_login2'], ));		
			Yii::app()->session->remove('errmsg_login2');	
		}
	}
}